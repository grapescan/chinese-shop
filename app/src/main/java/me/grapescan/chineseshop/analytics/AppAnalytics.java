package me.grapescan.chineseshop.analytics;

import android.support.annotation.CheckResult;

import me.grapescan.chineseshop.analytics.core.Events;
import me.grapescan.chineseshop.analytics.props.ScreenProperty;
import me.grapescan.chineseshop.analytics.props.Track;

@SuppressWarnings("unchecked")
public class AppAnalytics {
    public static void trackAppOpened() {
        Events.app("Open").track();
    }

    public static void trackAppClosed() {
        Events.app("Close").track();
    }

    public static void trackAppTimeout() {
        Events.app("Timeout").track();
    }

    public static void trackAppUpdate() {
        Events.app("Update").track();
    }

    public static void trackAppInstalled() {
        Events.app("Install").track();
    }

    @CheckResult
    public static ScreenProperty<Track> trackAppBrowse() {
        return Events.app("Browse");
    }

    @CheckResult
    public static ScreenProperty<Track> trackAppRate() {
        return Events.app("Rate");
    }


}
