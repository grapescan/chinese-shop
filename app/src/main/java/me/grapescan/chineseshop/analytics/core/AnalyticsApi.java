package me.grapescan.chineseshop.analytics.core;

import android.os.Handler;
import android.support.annotation.CheckResult;
import android.support.annotation.NonNull;

import me.grapescan.chineseshop.analytics.utils.BackgroundHandler;
import me.grapescan.chineseshop.app.App;
import me.grapescan.chineseshop.prefs.UserPrefs;


class AnalyticsApi {
    @CheckResult
    static EventBuilder event(@NonNull String eventName) {
        return new EventBuilderImpl(getImpl(), eventName);
    }

    public static Handler getAnalyticsHandler() {
        return BackgroundHandler.getInstance();
    }

    private static AnalyticsManager getImpl() {
        AnalyticsManager impl = AnalyticsApi.impl;
        if (impl == null) {
            synchronized (AnalyticsApi.class) {
                impl = AnalyticsApi.impl;
                if (impl == null) {
                    impl = AnalyticsApi.impl = new AnalyticsManager(App.Companion.getAppContext(),
                            getAnalyticsHandler(),
                            new UserPrefs());
                }
            }
        }
        return impl;
    }

    private static AnalyticsManager impl;
}
