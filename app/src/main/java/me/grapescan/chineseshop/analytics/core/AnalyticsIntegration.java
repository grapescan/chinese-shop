package me.grapescan.chineseshop.analytics.core;

import android.support.annotation.NonNull;

import java.util.Map;

import me.grapescan.chineseshop.analytics.utils.ImmutableMap;

public interface AnalyticsIntegration {
    void trackEvent(@NonNull String eventName,
                    @NonNull ImmutableMap<String, Object> eventProperties);

    void trackUserProperties(@NonNull String userId, @NonNull Map<String, Object> userProperties);

    void trackScreenView(String screenName);
}
