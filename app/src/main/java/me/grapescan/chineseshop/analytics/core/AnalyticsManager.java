package me.grapescan.chineseshop.analytics.core;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;

import java.util.HashMap;
import java.util.Map;

import me.grapescan.chineseshop.BuildConfig;
import me.grapescan.chineseshop.analytics.integrations.IntegrationsFactory;
import me.grapescan.chineseshop.analytics.utils.ImmutableMap;
import me.grapescan.chineseshop.prefs.UserPrefs;

public class AnalyticsManager {

    private static final String TAG = "AnalyticsManager";

    public AnalyticsManager(@NonNull Context context,
                            @NonNull Handler handler,
                            UserPrefs userPrefs) {
        this.context = context;
        this.handler = handler;
        this.userPrefs = userPrefs;
        initIntegrations();
    }

    private void initIntegrations() {
        for (String key : integrationsFactory.getAllKeys()) {
            initIntegration(key);
        }
    }

    private void initIntegration(String key) {
        if (integrations.get(key) != null) {
            return;
        }
        AnalyticsIntegration instance = integrationsFactory.getForKey(context, key);
        if (instance == null) {
            return;
        }
        instance.trackUserProperties(userPrefs.getUserId(), getUserProperties());
        integrations.put(key, instance);
    }

    private Map<String, Object> getUserProperties() {
        Map<String, Object> props = new HashMap<>();
        props.put("User ID", userPrefs.getUserId());
        return props;
    }

    public void track(@NonNull final String eventName,
                      @NonNull final ImmutableMap<String, Object> properties) {
        runOnAnalyticsThread(new Runnable() {
            @Override
            public void run() {
                trackImpl(eventName, properties);
            }
        });
    }

    private void runOnAnalyticsThread(Runnable runnable) {
        if (handler.getLooper() == Looper.myLooper()) {
            runnable.run();
        } else {
            handler.post(new RunnableWithTrace(runnable));
        }
    }

    private void trackImpl(@NonNull String eventName,
                           @NonNull ImmutableMap<String, Object> properties) {
        if (BuildConfig.DEBUG) {
            return; // skip events from debug builds
        }
        for (String key : integrations.keySet()) {
            dispatchTrack(key, eventName, properties);
        }
    }

    private void dispatchTrack(String key, String eventName,
                               ImmutableMap<String, Object> eventProperties) {
        AnalyticsIntegration instance = integrations.get(key);
        if (instance == null) {
            return;
        }
        instance.trackEvent(eventName, eventProperties);
    }

    private static class RunnableWithTrace implements Runnable {
        public RunnableWithTrace(Runnable runnable) {
            this.runnable = runnable;
            debugTrace = debugTrace();
        }

        @Override
        public void run() {
            try {
                runnable.run();
            } catch (RuntimeException e) {
                rethrow(e, debugTrace);
            }
        }

        private static Throwable debugTrace() {
            return new Throwable();
        }

        private static void rethrow(RuntimeException error, Throwable trace) {
            if (trace != null) {
                error = new RuntimeException(error);
                error.setStackTrace(trace.getStackTrace());
            }
            throw error;
        }

        private final Runnable runnable;
        private final Throwable debugTrace;
    }

    private final Context context;
    private final Handler handler;
    private UserPrefs userPrefs;

    private final HashMap<String, AnalyticsIntegration> integrations = new HashMap<>();
    private final IntegrationsFactory integrationsFactory = new IntegrationsFactory();

    private static final String LOG_TAG = "AnalyticsManager";
}
