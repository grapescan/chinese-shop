package me.grapescan.chineseshop.analytics.core;

import me.grapescan.chineseshop.analytics.props.ActionProperty;
import me.grapescan.chineseshop.analytics.props.ScreenProperty;
import me.grapescan.chineseshop.analytics.props.Track;

public interface EventBuilder extends
        ScreenProperty,
        ActionProperty,
        Track {
}
