package me.grapescan.chineseshop.analytics.core;

import java.util.HashMap;
import java.util.Map;

import me.grapescan.chineseshop.analytics.props.Property;
import me.grapescan.chineseshop.analytics.types.ScreenType;
import me.grapescan.chineseshop.analytics.utils.ImmutableMap;

class EventBuilderImpl implements EventBuilder {
    EventBuilderImpl(AnalyticsManager analytics, String eventName) {
        this.analytics = analytics;
        this.eventName = eventName;
    }

    private final AnalyticsManager analytics;
    private String eventName;
    private Map<String, Object> properties = new HashMap<>();

    @Override
    public EventBuilder putAction(String action) {
        return put("Action", action);
    }

    @Override
    public void track() {
        analytics.track(eventName, ImmutableMap.fromMap(properties));
    }

    private EventBuilder put(String key, Object value) {
        properties.put(key, value);
        return this;
    }

    @Override
    public Property putScreen(ScreenType screen) {
        return put("Screen", screen);
    }
}
