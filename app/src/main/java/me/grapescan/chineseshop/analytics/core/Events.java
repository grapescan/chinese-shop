package me.grapescan.chineseshop.analytics.core;

import android.support.annotation.CheckResult;


public class Events {
    @CheckResult
    public static EventBuilder app(String action) {
        return AnalyticsApi.event("App " + action).putAction(action);
    }
}


