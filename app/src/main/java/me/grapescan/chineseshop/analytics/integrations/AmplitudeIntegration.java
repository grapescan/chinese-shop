package me.grapescan.chineseshop.analytics.integrations;

import android.support.annotation.NonNull;
import android.util.Log;

import com.amplitude.api.Amplitude;
import com.amplitude.api.AmplitudeClient;
import com.amplitude.api.Identify;

import java.util.Map;

import me.grapescan.chineseshop.analytics.core.AnalyticsIntegration;
import me.grapescan.chineseshop.analytics.utils.ImmutableMap;
import me.grapescan.chineseshop.app.App;

class AmplitudeIntegration implements AnalyticsIntegration {
    public AmplitudeIntegration() {
        amplitude = Amplitude.getInstance();
        amplitude.initialize(App.getAppContext(), "78c9fc821f8402ec5801ef7d826a5b16");
        amplitude.enableForegroundTracking(App.getInstance());
        amplitude.enableLogging(true);
        amplitude.setLogLevel(Log.VERBOSE);
        amplitude.setEventUploadPeriodMillis(10000);
        amplitude.setEventUploadThreshold(5);
    }

    @Override
    public void trackEvent(@NonNull String eventName,
                           @NonNull ImmutableMap<String, Object> eventProperties) {
        amplitude.logEvent(eventName, eventProperties.toJSONObject());
    }

    @Override
    public void trackScreenView(String screenName) {

    }

    @Override
    public void trackUserProperties(@NonNull String userId, @NonNull Map<String, Object> userProperties) {
        Identify id = new Identify();
        for (String k : userProperties.keySet()) {
            id.set(k, String.valueOf(userProperties.get(k)));
        }
        amplitude.identify(id);
    }

    private final AmplitudeClient amplitude;
}
