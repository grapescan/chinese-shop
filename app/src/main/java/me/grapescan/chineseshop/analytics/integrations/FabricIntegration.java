package me.grapescan.chineseshop.analytics.integrations;

import android.support.annotation.NonNull;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.crashlytics.android.answers.CustomEvent;

import java.util.Map;

import me.grapescan.chineseshop.analytics.core.AnalyticsIntegration;
import me.grapescan.chineseshop.analytics.utils.ImmutableMap;

public class FabricIntegration implements AnalyticsIntegration {
    private final Answers answers;

    public FabricIntegration() {
        this.answers = Answers.getInstance();
    }

    @Override
    public void trackEvent(@NonNull String eventName, @NonNull ImmutableMap<String, Object> eventProperties) {
        CustomEvent event = new CustomEvent(eventName);
        for (String k : eventProperties.keys()) {
            event.putCustomAttribute(k, toString(eventProperties.get(k)));
        }
        answers.logCustom(event);
    }

    private String toString(Object value) {
        String s = String.valueOf(value);
        if (s.length() > 100) {
            s = s.substring(0, 100);
        }
        return s;
    }

    @Override
    public void trackUserProperties(@NonNull String userId, @NonNull Map<String, Object> userProperties) {
        for (String k : userProperties.keySet()) {
            Crashlytics.setString(k, String.valueOf(userProperties.get(k)));
        }
    }

    @Override
    public void trackScreenView(String screenName) {
        answers.logContentView(new ContentViewEvent()
                .putContentName(screenName)
                .putContentType("screen")
        );
    }
}
