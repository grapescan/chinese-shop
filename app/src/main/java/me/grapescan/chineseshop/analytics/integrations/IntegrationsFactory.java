package me.grapescan.chineseshop.analytics.integrations;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import me.grapescan.chineseshop.analytics.core.AnalyticsIntegration;

public class IntegrationsFactory {
    @Nullable
    public AnalyticsIntegration getForKey(@NonNull Context context, @NonNull String key) {
        switch (key) {
            case KEY_FABRIC:
                return new FabricIntegration();
            case KEY_AMPLITUDE:
                return new AmplitudeIntegration();
            default:
                return null;
        }
    }

    public String[] getAllKeys() {
        return new String[]{KEY_FABRIC, KEY_AMPLITUDE};
    }

    private static final String KEY_FABRIC = "fabric";
    private static final String KEY_AMPLITUDE = "amplitude";
}
