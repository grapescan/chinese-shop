package me.grapescan.chineseshop.analytics.props;

import android.support.annotation.CheckResult;

import me.grapescan.chineseshop.analytics.core.EventBuilder;

public interface ActionProperty<Next extends EventBuilder> extends Property {
    @CheckResult
    Next putAction(String action);
}
