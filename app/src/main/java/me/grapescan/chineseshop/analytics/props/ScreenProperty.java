package me.grapescan.chineseshop.analytics.props;

import android.support.annotation.CheckResult;

import me.grapescan.chineseshop.analytics.types.ScreenType;

public interface ScreenProperty<Next extends Property> extends Property {
    @CheckResult
    Next putScreen(ScreenType screen);
}