package me.grapescan.chineseshop.analytics.props;

public interface Track extends Property {
    void track();
}
