package me.grapescan.chineseshop.analytics.types;

public class ScreenType {

    private ScreenType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }

    private final String value;

    public static ScreenType main() {
        return new ScreenType("Main");
    }

    public static ScreenType productSet() {
        return new ScreenType("Product Set");
    }

    public static ScreenType productDetails() {
        return new ScreenType("Product Details");
    }

    public static ScreenType browser() {
        return new ScreenType("Browser");
    }
}
