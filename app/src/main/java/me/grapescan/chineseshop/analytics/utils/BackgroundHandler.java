package me.grapescan.chineseshop.analytics.utils;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;

public class BackgroundHandler extends Handler {
    public BackgroundHandler(String name) {
        super(createLooper(name));
    }

    /**
     * Handler singleton that runs on background thread.
     * Suitable for short tasks, for example database access.
     * Can't be used to access network.
     */
    public static BackgroundHandler getInstance() {
        return InstanceHolder.instance;
    }

    private static Looper createLooper(String name) {
        HandlerThread thread = new HandlerThread(name);
        thread.start();
        return thread.getLooper();
    }

    private static class InstanceHolder {
        static final BackgroundHandler instance = new BackgroundHandler("background-handler");
    }
}
