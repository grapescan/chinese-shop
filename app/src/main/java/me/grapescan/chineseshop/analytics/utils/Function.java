package me.grapescan.chineseshop.analytics.utils;

public interface Function<T, R> {
    R apply(T t);
}
