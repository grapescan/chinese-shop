package me.grapescan.chineseshop.analytics.utils;

import android.support.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class ImmutableMap<K, V> {
    public static <K, V> ImmutableMap<K, V> fromMap(Map<K, V> fromMap) {
        if (fromMap.size() == 0) {
            return empty();
        }
        ImmutableMap<K, V> map = new ImmutableMap<>(0, fromMap.size());
        for (Map.Entry<K, V> entry : fromMap.entrySet()) {
            map.mutableAppend(checkKey(entry.getKey()), entry.getValue());
        }
        return map;
    }

    @SuppressWarnings("unchecked")
    public static <K, V> ImmutableMap<K, V> empty() {
        return EMPTY;
    }

    public static <K, V> ImmutableMap<K, V> of(K key, V value) {
        ImmutableMap<K, V> map = new ImmutableMap<>(0, 1);
        map.mutableAppend(checkKey(key), value);
        return map;
    }

    public int size() {
        return size;
    }

    public List<K> keys() {
        return (List<K>) Collections.unmodifiableList(Arrays.asList(keys));
    }

    public List<V> values() {
        return (List<V>) Collections.unmodifiableList(Arrays.asList(values));
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public K keyAt(int index) {
        return (K) keys[index];
    }

    @SuppressWarnings("unchecked")
    public V valueAt(int index) {
        return (V) values[index];
    }

    public V get(K key) {
        int index = indexOf(key, keys, size);
        if (index < 0) {
            return null;
        }
        return valueAt(index);
    }

    public JSONObject toJSONObject() {
        try {
            return Utils.toJSONObject(this);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    public ImmutableMap<K, V> add(@NonNull K key, V value) {
        checkKey(key);
        int index = indexOf(key, keys, size);
        if (index < 0) {
            ImmutableMap<K, V> copy = copyWithExtraSpace(1);
            copy.mutableAppend(key, value);
            return copy;
        } else if (value == values[index]) {
            return this;
        } else {
            ImmutableMap<K, V> copy = copyWithExtraSpace(0);
            copy.mutableRemoveAt(index);
            copy.mutableAppend(key, value);
            return copy;
        }
    }

    public ImmutableMap<K, V> remove(K key) {
        int index = indexOf(key, keys, size);
        if (index < 0) {
            return this;
        } else {
            ImmutableMap<K, V> copy = copyWithExtraSpace(0);
            copy.mutableRemoveAt(index);
            return copy;
        }
    }

    public ImmutableMap<K, V> addAll(ImmutableMap<K, V> other) {
        return addAll(other, null);
    }

    public ImmutableMap<K, V> addAll(ImmutableMap<K, V> other, Predicate2<V, V> eq) {
        int[] indices = computeIndices(other);
        int newKeys = 0;
        for (int index : indices) {
            if (index < 0) {
                ++newKeys;
            }
        }

        ImmutableMap<K, V> copy = null;
        for (int i = 0; i < other.size; ++i) {
            K key = other.keyAt(i);
            V value = other.valueAt(i);
            int index = indices[i];
            if (index >= 0) {
                V myValue = valueAt(index);
                boolean equal = eq != null ? eq.test(myValue, value) : myValue == value;
                if (equal) {
                    continue;
                }
            }
            if (copy == null) {
                copy = copyWithExtraSpace(newKeys);
            }
            copy.mutableRemoveKey(key);
            copy.mutableAppend(key, value);
        }

        return copy != null ? copy : this;
    }

    public static <K> List<K> intersection(ImmutableMap<K, ?> a, ImmutableMap<K, ?> b) {
        ArrayList<K> intersection = new ArrayList<>();
        for (int i = 0; i < a.size; ++i) {
            int index = indexOf(a.keys[i], b.keys, b.size);
            if (index >= 0) {
                intersection.add(a.keyAt(i));
            }
        }
        return intersection;
    }

    public ImmutableMap<K, V> removeAll(Collection<K> keys) {
        ImmutableMap<K, V> result = this;
        for (K key : keys) {
            int index = indexOf(key, result.keys, result.size);
            if (index >= 0) {
                if (result == this) {
                    result = copyWithExtraSpace(0);
                }
                result.mutableRemoveAt(index);
            }
        }
        return result;
    }

    public ImmutableMap<K, V> mapKeys(Function<K, K> map) {
        ImmutableMap<K, V> copy = null;
        for (int i = 0; i < size; ++i) {
            K key = keyAt(i);
            K mappedKey = map.apply(key);
            if (!equal(key, mappedKey)) {
                if (copy == null) {
                    copy = copyWithExtraSpace(0);
                }
                copy.mutableRemoveKey(key);
                copy.mutableRemoveKey(mappedKey);
                if (mappedKey != null) {
                    copy.mutableAppend(mappedKey, valueAt(i));
                }
            }
        }
        return copy != null ? copy : this;
    }

    public ImmutableMap<K, V> mapValues(Function<V, V> map) {
        ImmutableMap<K, V> copy = null;
        int offset = 0;
        for (int i = 0; i < size; ++i) {
            V value = valueAt(i);
            V mappedValue = map.apply(value);
            if (!equal(value, mappedValue)) {
                if (copy == null) {
                    copy = copyWithExtraSpace(0);
                }
                copy.mutableRemoveAt(i - offset);
                copy.mutableAppend(keyAt(i), mappedValue);
                ++offset;
            }
        }
        return copy != null ? copy : this;
    }

    public ImmutableMap<K, V> filter(Predicate2<K, V> filter) {
        boolean[] removeAt = new boolean[size];
        int removeCount = 0;
        for (int i = 0; i < size; ++i) {
            if (!filter.test(keyAt(i), valueAt(i))) {
                removeAt[i] = true;
                ++removeCount;
            }
        }
        if (removeCount == 0) {
            return this;
        }
        ImmutableMap<K, V> copy = new ImmutableMap<>(0, size - removeCount);
        for (int i = 0; i < size; ++i) {
            if (!removeAt[i]) {
                copy.mutableAppend(keyAt(i), valueAt(i));
            }
        }
        return copy;
    }

    public void forEach(SideEffect2<K, V> forEach) {
        for (int i = 0; i < size; ++i) {
            forEach.run(keyAt(i), valueAt(i));
        }
    }

    private int[] computeIndices(ImmutableMap other) {
        int[] indices = new int[other.size];
        for (int i = 0; i < other.size; ++i) {
            indices[i] = indexOf(other.keys[i], keys, size);
        }
        return indices;
    }

    private void mutableRemoveAt(int index) {
        if (index < 0) {
            return;
        }
        System.arraycopy(keys, index + 1, keys, index, size - index - 1);
        System.arraycopy(values, index + 1, values, index, size - index - 1);
        --size;
        keys[size] = null;
        values[size] = null;
    }

    private void mutableRemoveKey(K key) {
        mutableRemoveAt(indexOf(key, keys, size));
    }

    private void mutableAppend(K key, V value) {
        keys[size] = key;
        values[size] = value;
        ++size;
    }

    private static int indexOf(Object value, Object[] array, int size) {
        for (int i = 0; i < size; ++i) {
            if (equal(value, array[i])) {
                return i;
            }
        }
        return -1;

    }

    private static boolean equal(Object a, Object b) {
        return a == null ? b == null : a.equals(b);
    }

    private static <K> K checkKey(K key) {
        if (key == null) {
            throw new IllegalArgumentException("key == null");
        }
        return key;
    }

    private ImmutableMap<K, V> copyWithExtraSpace(int extraSpace) {
        ImmutableMap<K, V> copy = new ImmutableMap<>(size, size + extraSpace);
        System.arraycopy(keys, 0, copy.keys, 0, size);
        System.arraycopy(values, 0, copy.values, 0, size);
        return copy;
    }

    private ImmutableMap(int size, int capacity) {
        this.size = size;
        keys = new Object[capacity];
        values = new Object[capacity];
    }

    private int size;
    private final Object[] keys;
    private final Object[] values;

    private static final ImmutableMap EMPTY = new ImmutableMap(0, 0);
}
