package me.grapescan.chineseshop.analytics.utils;

public interface Predicate2<K, V> {
    boolean test(K k, V v);
}
