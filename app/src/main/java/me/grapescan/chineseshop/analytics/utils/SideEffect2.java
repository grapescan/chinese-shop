package me.grapescan.chineseshop.analytics.utils;

public interface SideEffect2<K, V> {
    void run(K k, V v);
}
