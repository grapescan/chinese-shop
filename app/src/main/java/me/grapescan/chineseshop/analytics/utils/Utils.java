package me.grapescan.chineseshop.analytics.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Map;

public class Utils {
    public static JSONObject toJSONObject(ImmutableMap<?, ?> map) throws JSONException {
        JSONObject json = new JSONObject();
        for (int i = 0; i < map.size(); ++i) {
            json.put(map.keyAt(i).toString(), toJSON(map.valueAt(i)));
        }
        return json;
    }

    public static Object toJSON(Object o) throws JSONException {
        if (o == null) {
            return JSONObject.NULL;
        }
        if (o instanceof JSONArray || o instanceof JSONObject) {
            return o;
        }
        if (o.equals(JSONObject.NULL)) {
            return o;
        }
        if (o instanceof Collection) {
            return toJSONArray((Collection) o);
        } else if (o.getClass().isArray()) {
            return toJSONArray(o);
        }
        if (o instanceof Map) {
            return toJSONObject((Map) o);
        }
        if (o instanceof ImmutableMap) {
            return toJSONObject((ImmutableMap) o);
        }
        if (o instanceof Boolean ||
                o instanceof Byte ||
                o instanceof Character ||
                o instanceof Double ||
                o instanceof Float ||
                o instanceof Integer ||
                o instanceof Long ||
                o instanceof Short ||
                o instanceof String) {
            return o;
        }
        return o.toString();
    }

    public static JSONArray toJSONArray(Collection<?> collection) throws JSONException {
        JSONArray json = new JSONArray();
        for (Object item : collection) {
            json.put(toJSON(item));
        }
        return json;
    }

    public static JSONArray toJSONArray(Object array) throws JSONException {
        JSONArray json = new JSONArray();
        int length = Array.getLength(array);
        for (int i = 0; i < length; ++i) {
            json.put(toJSON(Array.get(array, i)));
        }
        return json;
    }

    public static JSONObject toJSONObject(Map<?, ?> map) throws JSONException {
        JSONObject json = new JSONObject();
        for (Map.Entry entry : map.entrySet()) {
            json.put(entry.getKey().toString(), toJSON(entry.getValue()));
        }
        return json;
    }
}
