package me.grapescan.chineseshop.api.aliexpress

import io.reactivex.Single
import me.grapescan.chineseshop.api.annotations.ApiPayload
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * @author Dmitry Soldatov
 */
interface AliexpressApi {
    /*    @POST("openapi/param2/2/portals.open/api.getPromotionProductDetail/{apiKey}?fields=productId,productTitle,productUrl,imageUrl,originalPrice,salePrice,localPrice,discount,evaluateScore,commission,commissionRate,30daysCommission,packageType,lotNum,validTime,storeUrl,volume,storeName,allImageUrls")
        fun getFavoritesProducts(@Path("apiKey") i: Int, @Query("language") str: String, @Query("localCurrency") str2: String, @Query("productId") str3: String): Call<GetPromotionProductDetailModel>
    */
    @POST("openapi/param2/2/portals.open/api.listHotProducts/{apiKey}")
    @ApiPayload(AliexpressPayload::class)
    fun getListHotProducts(@Path("apiKey") apiKey: Int, @Query("language") language: String, @Query("localCurrency") localCurrency: String, @Query("categoryId") categoryId: String): Single<HotProductsResponse>
/*
    @POST("openapi/param2/2/portals.open/api.listPromotionProduct/{apiKey}?fields=productId,productTitle,productUrl,imageUrl,originalPrice,localPrice,evaluateScore,validTime,storeUrl,volume,storeName,allImageUrls&pageSize=40")
    fun getListPromotionProduct(@Path("apiKey") i: Int, @Query("language") str: String, @Query("localCurrency") str2: String, @Query("keywords") str3: String, @Query("pageNo") str4: String, @Query("originalPriceFrom") str5: String, @Query("categoryId") str6: String, @Query("originalPriceTo") str7: String, @Query("sort") str8: String, @Query("volumeFrom") str9: String): Call<ListPromotionProduct>

    @POST("openapi/param2/2/portals.open/api.getPromotionLinks/{apiKey}?fields=promotionUrl&trackingId=henzoteam")
    fun getPromotionLinks(@Path("apiKey") i: Int, @Query("urls") str: String): Call<ResponseGetPromotionUrl>

    @POST("openapi/param2/2/portals.open/api.getPromotionProductDetail/{apiKey}?fields=productId,productTitle,productUrl,imageUrl,originalPrice,salePrice,localPrice,discount,evaluateScore,commission,commissionRate,30daysCommission,packageType,lotNum,validTime,storeUrl,volume,storeName,allImageUrls")
    fun getPromotionProductDetail(@Path("apiKey") i: Int, @Query("language") str: String, @Query("localCurrency") str2: String, @Query("productId") str3: String): Call<GetPromotionProductDetailModel>
*/
}