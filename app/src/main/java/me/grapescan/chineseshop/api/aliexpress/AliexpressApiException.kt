package me.grapescan.chineseshop.api.aliexpress

import me.grapescan.chineseshop.api.base.BaseApiException

/**
 * @author Dmitry Soldatov
 */
class AliexpressApiException(private val errorCode: Int?) : BaseApiException() {

    override fun toString(): String {
        return "AliexpressApiException (errorCode=$errorCode)"
    }
}