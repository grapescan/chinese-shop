package me.grapescan.chineseshop.api.aliexpress

import com.google.gson.annotations.SerializedName
import me.grapescan.chineseshop.api.base.BasePayload
import java.io.Serializable

/**
 * @author Dmitry Soldatov
 */
class AliexpressPayload<T> : BasePayload<T>, Serializable {

    override val payloadResult: T? get() = result
    override val successResult: Boolean get() = result != null

    @SerializedName("result")
    var result: T? = null

    @SerializedName("errorCode")
    var errorCode: Int? = null
}