package me.grapescan.chineseshop.api.aliexpress

import com.google.gson.annotations.SerializedName
import me.grapescan.chineseshop.api.aliexpress.model.HotProduct

/**
 * @author Dmitry Soldatov
 */
data class HotProductsResponse(@SerializedName("products") val products: List<HotProduct> = emptyList())