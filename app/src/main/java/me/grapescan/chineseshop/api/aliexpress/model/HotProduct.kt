package me.grapescan.chineseshop.api.aliexpress.model

import com.google.gson.annotations.SerializedName

/**
 * @author Dmitry Soldatov
 */
data class HotProduct(
        @SerializedName("imageUrl") val imageUrl: String? = null,
        @SerializedName("localPrice") val localPrice: String? = null,
        @SerializedName("productId") val productId: Long = 0,
        @SerializedName("productTitle") val productTitle: String? = null,
        @SerializedName("productUrl") val productUrl: String? = null,
        @SerializedName("salePrice") val salePrice: String? = null,
        @SerializedName("validTime") val validTime: String? = null,
        @SerializedName("volume") val volume: String? = null
)