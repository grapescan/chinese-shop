package me.grapescan.chineseshop.api.annotations

import kotlin.reflect.KClass

object AnnotationUtils {

    private val defaultPayloadType = Any::class

    fun Array<out Annotation>?.hasAnnotation(kClass: KClass<*>): Boolean {
        return this?.any { it.annotationClass == kClass } ?: false
    }

    fun Array<out Annotation>?.hasPayloadAnnotation(): PayloadAnnotationResult {
        return this?.firstOrNull { it.annotationClass == ApiPayload::class }
                ?.let { PayloadAnnotationResult(true, (it as ApiPayload).value) }
                ?: PayloadAnnotationResult(false, defaultPayloadType)
    }

    class PayloadAnnotationResult(val hasAnnotation: Boolean, val payload: KClass<*>)
}