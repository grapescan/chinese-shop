package me.grapescan.chineseshop.api.annotations

import kotlin.reflect.KClass

@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
annotation class ApiPayload(val value: KClass<*>)