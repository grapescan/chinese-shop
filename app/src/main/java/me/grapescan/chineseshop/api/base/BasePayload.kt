package me.grapescan.chineseshop.api.base

interface BasePayload<out T> {
    val payloadResult: T?
    val successResult: Boolean
}