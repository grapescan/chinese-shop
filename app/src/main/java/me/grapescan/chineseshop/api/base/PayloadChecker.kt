package me.grapescan.chineseshop.api.base

import me.grapescan.chineseshop.api.aliexpress.AliexpressApiException
import me.grapescan.chineseshop.api.aliexpress.AliexpressPayload

object PayloadChecker {
    fun checkPayload(payload: BasePayload<*>) {
        if (!payload.successResult) {
            when (payload) {
                is AliexpressPayload -> throw AliexpressApiException(payload.errorCode)
            }
        }
    }
}