package me.grapescan.chineseshop.api.base

import com.google.gson.Gson
import me.grapescan.chineseshop.api.annotations.AnnotationUtils.hasAnnotation
import me.grapescan.chineseshop.api.annotations.AnnotationUtils.hasPayloadAnnotation
import me.grapescan.chineseshop.api.annotations.ElementByElement
import me.grapescan.chineseshop.core.Optional
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.apache.commons.lang3.reflect.TypeUtils
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type
import kotlin.reflect.KClass

/**
 * @author Dmitry Soldatov
 */
class PayloadConverterFactory : Converter.Factory {

    private val gsonFactory: GsonConverterFactory
    private val listType: Type
    private val optionalType: Type

    constructor() {
        gsonFactory = GsonConverterFactory.create()
        listType = TypeUtils.wrap(List::class.java).type
        optionalType = TypeUtils.wrap(Optional::class.java).type
    }

    constructor(gson: Gson) {
        gsonFactory = GsonConverterFactory.create(gson)
        listType = TypeUtils.wrap(List::class.java).type
        optionalType = TypeUtils.wrap(Optional::class.java).type
    }

    override fun responseBodyConverter(type: Type, annotations: Array<out Annotation>?, retrofit: Retrofit?): Converter<ResponseBody, *>? {
        val result = annotations.hasPayloadAnnotation()

        if (result.hasAnnotation) {
            val innerType: Type
            val wrappedType: ParameterizedType
            when {
                annotations.hasAnnotation(ElementByElement::class) -> {
                    innerType = wrapIn(List::class, type)
                    wrappedType = wrapIn(result.payload, innerType)
                }
                type.isOptional() -> {
                    innerType = type
                    wrappedType = wrapIn(result.payload, type.innerType())
                }
                else -> {
                    innerType = type
                    wrappedType = wrapIn(result.payload, type)
                }
            }
            return createPayloadConverter(wrappedType, innerType, annotations, retrofit)
        } else {
            return createBaseConverter(type, annotations, retrofit)
        }
    }

    override fun requestBodyConverter(type: Type?, parameterAnnotations: Array<out Annotation>?, methodAnnotations: Array<out Annotation>?, retrofit: Retrofit?): Converter<*, RequestBody>? {
        return gsonFactory.requestBodyConverter(type, parameterAnnotations, methodAnnotations, retrofit)
    }

    private fun createPayloadConverter(wrappedType: Type, innerType: Type, annotations: Array<out Annotation>?, retrofit: Retrofit?): Converter<ResponseBody, Any> {
        val converter: Converter<ResponseBody, BasePayload<*>> = gsonFactory.responseBodyConverter(wrappedType, annotations, retrofit) as Converter<ResponseBody, BasePayload<*>>
        return Converter {
            val payload = converter.convert(it)
            PayloadChecker.checkPayload(payload)

            if (payload.payloadResult == null && innerType.isList()) {
                emptyList<Any>()
            } else if (innerType.isOptional()) {
                Optional.of(payload.payloadResult)
            } else {
                payload.payloadResult
            }
        }
    }

    private fun createBaseConverter(type: Type, annotations: Array<out Annotation>?, retrofit: Retrofit?): Converter<ResponseBody, BasePayload<*>> {
        val converter: Converter<ResponseBody, BasePayload<*>> = gsonFactory.responseBodyConverter(type, annotations, retrofit) as Converter<ResponseBody, BasePayload<*>>
        return Converter {
            val payload = converter.convert(it)
            PayloadChecker.checkPayload(payload)
            payload
        }
    }

    private fun wrapIn(outerClass: KClass<*>, type: Type?): ParameterizedType {
        return TypeUtils.parameterize(outerClass.java, type)
    }

    private fun Type.isList() = getRawType(this) == listType

    private fun Type.isOptional() = getRawType(this) == optionalType

    private fun Type.innerType() = getParameterUpperBound(0, this as ParameterizedType)

}
