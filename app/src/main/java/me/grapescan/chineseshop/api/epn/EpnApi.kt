package me.grapescan.chineseshop.api.epn

import io.reactivex.Single
import me.grapescan.chineseshop.api.epn.model.EpnRequest
import me.grapescan.chineseshop.api.epn.model.EpnResponse
import retrofit2.http.Body
import retrofit2.http.POST

/**
 * @author Dmitry Soldatov
 */
interface EpnApi {
    @POST("json")
    fun request(@Body request: EpnRequest): Single<EpnResponse>
}