package me.grapescan.chineseshop.api.epn.model

import com.google.gson.annotations.SerializedName

/**
 * @author Dmitry Soldatov
 */
data class EpnOffer(
        @SerializedName("id") val id: String,
        @SerializedName("id_category") val categoryId: String,
        @SerializedName("name") val name: String,
        @SerializedName("picture") val imageUrl: String,
        @SerializedName("all_images") val allImageUrls: List<String>,
        @SerializedName("price") val price: Double,
        @SerializedName("sale_price") val salePrice: Double,
        @SerializedName("currency") val currency: String,
        @SerializedName("description") val description: String,
        @SerializedName("product_id") val productId: String,
        @SerializedName("url") val url: String,
        @SerializedName("lang") val lang: String,
        @SerializedName("commission_rate") val commissionRate: Double,
        @SerializedName("store_id") val storeId: String,
        @SerializedName("store_title") val storeTitle: String,
        @SerializedName("prices") val prices: Map<String, String>,
        @SerializedName("sale_prices") val salePrices: Map<String, String>
)