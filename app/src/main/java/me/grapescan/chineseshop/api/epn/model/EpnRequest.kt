package me.grapescan.chineseshop.api.epn.model

import com.google.gson.annotations.SerializedName

/**
 * @author Dmitry Soldatov
 */
data class EpnRequest(
        @SerializedName("user_api_key") val userApiKey: String = "2cf54683595bd58b0e99fd6a75dc6bbc",
        @SerializedName("user_hash") val userHash: String = "pbwjn57s84bpwk79oji6c2vdq954efzs", // deep link hash
        @SerializedName("api_version") val clientLibraryVersion: String = "2",
        @SerializedName("requests") val requestsToProcess: List<Map<String, String>>
)
