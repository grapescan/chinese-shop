package me.grapescan.chineseshop.api.epn.model

import com.google.gson.annotations.SerializedName

/**
 * @author Dmitry Soldatov
 */
data class EpnResponse(
        @SerializedName("identified_as") val identifiedAs: String,
        @SerializedName("error") val error: String,
        @SerializedName("results") val results: List<EpnResult>
)