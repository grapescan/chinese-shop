package me.grapescan.chineseshop.api.epn.model

import com.google.gson.annotations.SerializedName

/**
 * @author Dmitry Soldatov
 */
data class EpnResult(
        @SerializedName("offer") val offer: EpnOffer?
)