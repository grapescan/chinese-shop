package me.grapescan.chineseshop.app

import android.app.Application
import android.content.Context
import android.util.Log
import com.crashlytics.android.Crashlytics
import io.fabric.sdk.android.Fabric
import io.reactivex.plugins.RxJavaPlugins

class App : Application() {

    companion object {
        @JvmStatic
        lateinit var appContext: Context
            private set

        @JvmStatic
        lateinit var instance: Application
            private set
    }

    override fun onCreate() {
        super.onCreate()
        Fabric.with(this, Crashlytics())
        instance = this
        appContext = applicationContext
        RxJavaPlugins.setErrorHandler { error ->
            Log.d("UnknownError", "unhandled error", error)
            Crashlytics.logException(error)
        }
    }
}