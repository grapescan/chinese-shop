package me.grapescan.chineseshop.app

import android.app.Activity
import org.codechimp.apprater.AppRater

class AppRaterUtil {
    companion object {
        private const val APP_RATE_DAYS_UNTIL_PROMPT = 2
        private const val APP_RATE_USES_UNTIL_PROMPT = 5
        private const val APP_RATE_DAYS_UNTIL_REMIND = 2

        fun onAppLaunch(activity: Activity) {
            AppRater.setNumDaysForRemindLater(APP_RATE_DAYS_UNTIL_REMIND)
            AppRater.app_launched(activity, APP_RATE_DAYS_UNTIL_PROMPT, APP_RATE_USES_UNTIL_PROMPT)
        }
    }
}

