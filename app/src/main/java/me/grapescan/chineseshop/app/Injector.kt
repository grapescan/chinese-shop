package me.grapescan.chineseshop.app

import me.grapescan.chineseshop.api.epn.EpnApi
import me.grapescan.chineseshop.data.ProductRepositoryImpl
import me.grapescan.chineseshop.prefs.UserPrefs
import me.grapescan.chineseshop.ui.main.MainPresenter
import me.grapescan.chineseshop.ui.product.details.ProductDetailsPresenter
import me.grapescan.chineseshop.ui.product.list.ProductListPresenter
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object Injector {

    val userPrefs by lazy { UserPrefs() }
    private val epnClient by lazy {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        val httpClient = OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .build()
        Retrofit.Builder()
                .baseUrl("http://api.epn.bz/")
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(EpnApi::class.java)
    }
    private val productRepository by lazy { ProductRepositoryImpl(epnClient, userPrefs) }
    val mainPresenter by lazy { MainPresenter(productRepository) }
    val productListPresenter by lazy { ProductListPresenter(productRepository) }
    val productDetailsPresenter by lazy { ProductDetailsPresenter(productRepository) }


}