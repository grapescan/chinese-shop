package ru.tinkoff.mortgage.sdk.delegates

import android.os.Bundle
import android.os.Parcelable
import java.io.Serializable
import kotlin.reflect.KProperty

/**
 * @author Dmitry Soldatov
 */
class IntArg(private val defaultValue: Int = 0) {
    operator fun getValue(thisRef: Bundle, property: KProperty<*>): Int {
        return thisRef.getInt(property.name, defaultValue)
    }

    operator fun setValue(thisRef: Bundle, property: KProperty<*>, value: Int) {
        thisRef.putInt(property.name, value)
    }
}

class BooleanArg(private val defaultValue: Boolean = false) {
    operator fun getValue(thisRef: Bundle, property: KProperty<*>): Boolean {
        return thisRef.getBoolean(property.name, defaultValue)
    }

    operator fun setValue(thisRef: Bundle, property: KProperty<*>, value: Boolean) {
        thisRef.putBoolean(property.name, value)
    }
}

class StringArg {
    operator fun getValue(thisRef: Bundle, property: KProperty<*>): String? {
        return thisRef.getString(property.name)
    }

    operator fun setValue(thisRef: Bundle, property: KProperty<*>, value: String?) {
        thisRef.putString(property.name, value)
    }
}

class SerializableArg<T : Serializable> {
    operator fun getValue(thisRef: Bundle, property: KProperty<*>): T {
        return thisRef.getSerializable(property.name) as T
    }

    operator fun setValue(thisRef: Bundle, property: KProperty<*>, value: T) {
        thisRef.putSerializable(property.name, value as Serializable)
    }
}

class ParcelableArg<T : Parcelable> {
    operator fun getValue(thisRef: Bundle, property: KProperty<*>): T {
        return thisRef.getParcelable(property.name) as T
    }

    operator fun setValue(thisRef: Bundle, property: KProperty<*>, value: T) {
        thisRef.putParcelable(property.name, value as Parcelable)
    }
}