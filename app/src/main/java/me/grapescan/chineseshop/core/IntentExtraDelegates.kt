package ru.tinkoff.mortgage.sdk.delegates

import android.content.Intent
import android.os.Parcelable
import java.io.Serializable
import kotlin.reflect.KProperty

/**
 * @author Dmitry Soldatov
 */
class IntExtra(private val defaultValue: Int = 0) {
    operator fun getValue(thisRef: Intent, property: KProperty<*>): Int {
        return thisRef.getIntExtra(property.name, defaultValue)
    }

    operator fun setValue(thisRef: Intent, property: KProperty<*>, value: Int) {
        thisRef.putExtra(property.name, value)
    }
}

class BooleanExtra(private val defaultValue: Boolean = false) {
    operator fun getValue(thisRef: Intent, property: KProperty<*>): Boolean {
        return thisRef.getBooleanExtra(property.name, defaultValue)
    }

    operator fun setValue(thisRef: Intent, property: KProperty<*>, value: Boolean) {
        thisRef.putExtra(property.name, value)
    }
}

class StringExtra {
    operator fun getValue(thisRef: Intent, property: KProperty<*>): String? {
        return thisRef.getStringExtra(property.name)
    }

    operator fun setValue(thisRef: Intent, property: KProperty<*>, value: String?) {
        thisRef.putExtra(property.name, value)
    }
}

class SerializableExtra<T : Serializable> {
    operator fun getValue(thisRef: Intent, property: KProperty<*>): T {
        return thisRef.getSerializableExtra(property.name) as T
    }

    operator fun setValue(thisRef: Intent, property: KProperty<*>, value: T) {
        thisRef.putExtra(property.name, value as Serializable)
    }
}

class ParcelableExtra<T : Parcelable> {
    operator fun getValue(thisRef: Intent, property: KProperty<*>): T {
        return thisRef.getParcelableExtra(property.name) as T
    }

    operator fun setValue(thisRef: Intent, property: KProperty<*>, value: T) {
        thisRef.putExtra(property.name, value as Parcelable)
    }
}
