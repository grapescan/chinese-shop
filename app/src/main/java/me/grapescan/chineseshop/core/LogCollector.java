package me.grapescan.chineseshop.core;

import android.content.Context;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class LogCollector {
    private static final int LOG_LINE_COUNT = 1000;
    private static final String TAG = LogCollector.class.getSimpleName();

    public File getLogFile(Context context, String logHeader) {
        // may be use external storage should be used instead of cache dir
        File cacheDir = context.getExternalCacheDir();
        if (cacheDir == null) {
            cacheDir = context.getCacheDir();
            if (cacheDir == null) {
                return null;
            }
        }
        cacheDir.mkdirs();
        File file = new File(cacheDir, "support-" + new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + ".zip");

        FileOutputStream stream;
        try {
            stream = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            Log.e(TAG, "Can't collect logs", e);
            return null;
        }
        ZipOutputStream zipOutputStream = new ZipOutputStream(new BufferedOutputStream(stream));
        OutputStreamWriter writer = new OutputStreamWriter(zipOutputStream);
        try {
            try {
                zipOutputStream.putNextEntry(new ZipEntry("log.txt"));
                writer.append(logHeader).append("\n\n");
                collectLogs(writer);
            } finally {
                writer.close();
            }
        } catch (IOException e) {
            Log.e(TAG, "Can't collect logs", e);
            return null;
        }
        return file;
    }

    public static String getLog() {
        try {
            StringWriter writer = new StringWriter();
            collectLogs(writer);
            return writer.toString();
        } catch (IOException e) {
            return e.toString();
        }
    }

    private static void collectLogs(Writer writer) throws IOException {
        Process process = Runtime.getRuntime().exec("logcat -d *:D");
        BufferedReader bufferedReader =
                new BufferedReader(new InputStreamReader(process.getInputStream()));
        Ring logs = new Ring(LOG_LINE_COUNT);
        try {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                logs.push(line);
            }
        } finally {
            bufferedReader.close();
        }


        String separator = System.getProperty("line.separator");
        for (String logLine : logs) {
            writer.append(logLine);
            writer.append(separator);
        }
    }

    private static class Ring implements Iterable<String> {
        private final String[] items;
        private int start;
        private int end;

        public Ring(int size) {
            items = new String[size + 1];
        }

        public void push(String line) {
            items[end] = line;
            end++;
            if (end == items.length) {
                end = 0;
            }

            if (start == end) {
                start++;
                if (start == items.length) {
                    start = 0;
                }
            }
        }

        @Override
        public Iterator<String> iterator() {
            return new Iterator<String>() {
                private int current = start - 1;

                @Override
                public boolean hasNext() {
                    current++;
                    if (current == items.length) {
                        current = 0;
                    }

                    return current != end;
                }

                @Override
                public String next() {
                    return items[current];
                }

                @Override
                public void remove() {
                }
            };
        }
    }
}
