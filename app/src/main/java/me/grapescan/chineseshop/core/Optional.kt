package me.grapescan.chineseshop.core

class Optional<out T> private constructor(private val nullableValue: T?) {

    val value: T
        get() = nullableValue ?: throw IllegalStateException("Value must be NotNull")

    val isNull: Boolean
        get() = nullableValue == null

    @get:JvmName("hasValue")
    val hasValue: Boolean
        get() = nullableValue != null

    companion object {
        @JvmStatic
        fun <T> of(value: T?) = Optional(value)

        @JvmStatic
        fun empty() = Optional(null)
    }
}