package me.grapescan.chineseshop.core

import android.support.annotation.ColorRes
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView

/**
 * @author Dmitry Soldatov
 */
fun RecyclerView.ViewHolder.getColor(@ColorRes colorResId: Int) = ContextCompat.getColor(itemView.context, colorResId)