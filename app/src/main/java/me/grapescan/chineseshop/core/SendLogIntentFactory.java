package me.grapescan.chineseshop.core;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.util.Log;

import java.io.File;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import me.grapescan.chineseshop.R;
import me.grapescan.chineseshop.app.Injector;
import me.grapescan.chineseshop.prefs.UserPrefs;


public class SendLogIntentFactory {
    public static final String TAG = SendLogIntentFactory.class.getSimpleName();
    public static final long SD_LOCKED = -1;
    public static final String APP_FOLDER = "/birthdays/";

    private static SendLogIntentFactory instance;
    private UserPrefs userPrefs = Injector.INSTANCE.getUserPrefs();

    Context context;
    LogCollector logCollector;

    private SendLogIntentFactory(Context c) {
        context = c.getApplicationContext();
        logCollector = new LogCollector();
    }

    public static SendLogIntentFactory getInstance(Context c) {
        if (instance == null) {
            instance = new SendLogIntentFactory(c);
        }
        return instance;
    }

    public Intent createSendToIntent() {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        populateIntent(intent);
        String actionSend = context.getResources().getString(R.string.support_action_send);
        return Intent.createChooser(intent, actionSend);
    }

    public Intent createGMailIntent() {
        Intent intent = context.getPackageManager().getLaunchIntentForPackage("com.google.an‌​droid.gm");
        if (intent == null) {
            intent = findGmailIntent();
        }
        if (intent != null) {
            populateIntent(intent);
        }
        return intent;
    }

    private Intent findGmailIntent() {
        final Intent intent = new Intent(android.content.Intent.ACTION_SEND);
        intent.setType("text/plain");
        final PackageManager pm = context.getPackageManager();
        final List<ResolveInfo> matches = pm.queryIntentActivities(intent, 0);
        ResolveInfo best = null;
        for (final ResolveInfo info : matches)
            if (info.activityInfo.packageName.endsWith(".gm") ||
                    info.activityInfo.name.toLowerCase().contains("gmail")) best = info;
        if (best != null) {
            intent.setClassName(best.activityInfo.packageName, best.activityInfo.name);
            return intent;
        } else {
            return null;
        }
    }

    private void populateIntent(Intent intent) {
        intent.setAction(Intent.ACTION_SENDTO);
        String address = context.getResources().getString(R.string.support_email);
        intent.setData(Uri.parse("mailto:" + address));

        String mailTitle = context.getResources().getString(R.string.support_mail_title_help);
        intent.putExtra(Intent.EXTRA_SUBJECT, mailTitle + " " + userPrefs.getUserId());

        String logHeader = getUserInfo();

        File file = logCollector.getLogFile(context, logHeader);
        if (file != null) {
            Uri uri = FileProvider.getUriForFile(context,
                    context.getApplicationContext().getPackageName() + ".provider",
                    file);
            if (intent.getComponent() != null) {
                context.grantUriPermission(intent.getComponent().getPackageName(), uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
            }
            intent.putExtra(Intent.EXTRA_STREAM, uri);
        }
    }

    private String getUserInfo() {
        StringBuilder builder = new StringBuilder();

        builder.append("Device: ").append(getDeviceModelName()).append("\n");

        try {
            PackageManager packageManager = context.getPackageManager();
            assert packageManager != null;

            PackageInfo packageInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
            assert packageInfo != null;

            String versionString = packageInfo.versionName + " (" + packageInfo.versionCode + ")";
            builder.append("App Version: ").append(versionString).append("\n");
            builder.append("User ID: ").append(userPrefs.getUserId()).append("\n");
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "Error while getting user info", e);
        }

        builder.append("OS Version: ").append(Build.VERSION.RELEASE).append("\n");


        String freeExtMemory;
        if (isExternalStorageAvailable()) {
            long size = getFreeExtMemory(getAppFolder());
            freeExtMemory = Long.toString(size);
        } else {
            freeExtMemory = " external storage is not available";
        }

        builder.append("Free Space: ").append(freeExtMemory).append("\n");

        builder.append("Time zone: ")
                .append(TimeZone.getDefault().getID())
                .append(" ")
                .append(TimeZone.getDefault().getDisplayName(Locale.ENGLISH))
                .append("\n");

        return builder.toString();
    }

    private String getDeviceModelName() {
        return cleanDeviceString(Build.MANUFACTURER) + " " + cleanDeviceString(Build.MODEL);
    }

    private String cleanDeviceString(String input) {
        return (input.replaceAll("[\\W]", "_").toLowerCase(Locale.US));
    }

    private static long getFreeExtMemory(File file) {
        long free = 0;
        String path = file.getAbsolutePath();
        if (file == null || path.indexOf("null") >= 0) {
            free = SD_LOCKED;
        } else {
            StatFs statFs = new StatFs(file.getAbsolutePath());
            free = ((long) statFs.getAvailableBlocks() * statFs.getBlockSize());
        }
        return free;
    }

    private static boolean isExternalStorageAvailable() {
        File externalStorageDirectory = Environment.getExternalStorageDirectory();
        if (externalStorageDirectory == null) {
            return false;
        }
        File fileDir = new File(externalStorageDirectory + APP_FOLDER);
        return fileDir.exists() || fileDir.mkdirs();
    }

    public static File getAppFolder() {
        File fileDir = null;
        try {
            fileDir = new File(Environment.getExternalStorageDirectory() + APP_FOLDER);
        } catch (NullPointerException e) {
            Log.e(TAG, "Failed to open app folder", e);
            try {
                fileDir = new File(getExtSDCard());
            } catch (NullPointerException e2) {
                Log.e(TAG, "Failed to open SC card folder", e);
            }
        }
        if (null != fileDir && !fileDir.exists()) { // create if not exists
            fileDir.mkdir();
        }

        if (null != fileDir && (!fileDir.exists() || TextUtils.isEmpty(fileDir.getAbsolutePath()))) {
            try {
                fileDir = new File(getExtSDCard() + APP_FOLDER);
            } catch (NullPointerException e) {
                Log.e(TAG, "Failed to open app folder", e);
                fileDir = new File(getExtSDCard());
            }

            if (!fileDir.exists()) { // create if not exists
                fileDir.mkdir();
            }
        }
        return fileDir;
    }

    @SuppressLint("DefaultLocale")
    private static String getExtSDCard() {
        File[] files = new File("/mnt").listFiles();
        String sdcard = Environment.getExternalStorageDirectory().getAbsolutePath().toLowerCase();
        String file;

        for (int i = 0; i < files.length; i++) {
            file = files[i].getAbsolutePath().toLowerCase();
            if (!file.equalsIgnoreCase(sdcard) && file.contains(sdcard)) {
                return file;
            }
        }

        return null;
    }
}
