package me.grapescan.chineseshop.core

import android.content.Context
import android.net.Uri
import android.support.customtabs.CustomTabsIntent
import android.support.v4.content.ContextCompat
import me.grapescan.chineseshop.R

/**
 * @author Dmitry Soldatov
 */
fun Uri.open(context: Context, color: Int = ContextCompat.getColor(context, R.color.colorPrimary)) {
    CustomTabsIntent.Builder()
            .setToolbarColor(color)
            .build()
            .launchUrl(context, this)
}