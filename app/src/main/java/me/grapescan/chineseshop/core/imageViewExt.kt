package me.grapescan.chineseshop.core

import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target

/**
 * @author Dmitry Soldatov
 */
fun ImageView.centerInside(url: String, callback: (Bitmap) -> Unit = {}) = loadUrl(url, RequestOptions.centerInsideTransform(), callback)

fun ImageView.centerCrop(url: String, callback: (Bitmap) -> Unit = {}) = loadUrl(url, RequestOptions.centerCropTransform(), callback)

fun ImageView.loadUrl(url: String, options: RequestOptions = RequestOptions(), callback: (Bitmap) -> Unit) {
    Glide.with(this)
            .load(url)
            .apply(options)
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean) = false

                override fun onResourceReady(resource: Drawable, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                    callback.invoke((resource as BitmapDrawable).bitmap)
                    return false
                }

            })
            .into(this)
}