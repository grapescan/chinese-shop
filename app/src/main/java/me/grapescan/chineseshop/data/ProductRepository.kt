package me.grapescan.chineseshop.data

import io.reactivex.Observable
import io.reactivex.Single
import me.grapescan.chineseshop.ui.model.Product
import me.grapescan.chineseshop.ui.model.ProductSet

/**
 * @author Dmitry Soldatov
 */
interface ProductRepository {
    fun getProductSets(): Observable<List<RawProductSet>>
    fun getProductSet(id: String): Single<ProductSet>
    fun getProduct(id: String): Single<Product>
}