package me.grapescan.chineseshop.data

data class RawProductSet(
        val id: String,
        val name: String,
        val description: String,
        val coverImageUrl: String,
        val items: List<RawProduct>
)

data class RawProduct(
        val id: String,
        val promoUrl: String,
        val name: String,
        val description: String
)