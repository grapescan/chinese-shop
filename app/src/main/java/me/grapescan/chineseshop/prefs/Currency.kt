package me.grapescan.chineseshop.prefs

data class Currency(
        val code: String,
        val symbol: String
)