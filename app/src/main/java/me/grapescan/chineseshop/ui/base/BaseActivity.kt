package me.grapescan.chineseshop.ui.base

import android.support.v7.app.AppCompatActivity

/**
 * @author Dmitry Soldatov
 */
abstract class BaseActivity<T> : AppCompatActivity() {

    protected abstract val presenter: Presenter<T>

    override fun onResume() {
        super.onResume()
        presenter.attachView(this as T)
    }

    override fun onPause() {
        presenter.detachView()
        super.onPause()
    }
}