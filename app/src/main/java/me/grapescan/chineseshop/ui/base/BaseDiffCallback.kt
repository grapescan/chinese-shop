package me.grapescan.chineseshop.ui.base

import android.support.v7.util.DiffUtil

/**
 * @author Dmitry Soldatov
 */
class BaseDiffCallback<T> : DiffUtil.ItemCallback<T>() {
    override fun areItemsTheSame(oldItem: T, newItem: T) = oldItem == newItem

    override fun areContentsTheSame(oldItem: T, newItem: T) = oldItem == newItem
}