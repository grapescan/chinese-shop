package me.grapescan.chineseshop.ui.base

import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class Presenter<T> {

    protected var view: T? = null
    private var compositeDisposable = CompositeDisposable()

    open fun attachView(viewParam: T) {
        this.view = viewParam
    }

    open fun detachView() {
        view = null
        disposeAll()
    }

    protected fun addSubscription(subscription: Disposable) {
        compositeDisposable.add(subscription)
    }

    protected fun removeSubscription(subscription: Disposable) {
        compositeDisposable.remove(subscription)
    }

    protected fun Disposable.disposeOnDetach(): Disposable {
        compositeDisposable.add(this)
        return this
    }

    protected fun disposeAll() {
        compositeDisposable.dispose()
        compositeDisposable = CompositeDisposable()
    }

    protected inline fun <T> Single<T>.subscribeUntilDetach(noinline onNext: (T) -> Unit) {
        subscribe(onNext, {}).disposeOnDetach()
    }

    protected inline fun <T> Single<T>.subscribeUntilDetach(noinline onNext: (T) -> Unit, noinline onError: (Throwable) -> Unit) {
        subscribe(onNext, onError).disposeOnDetach()
    }

    protected inline fun <T> Observable<T>.subscribeUntilDetach(noinline onNext: (T) -> Unit, noinline onError: (Throwable) -> Unit) {
        subscribe(onNext, onError).disposeOnDetach()
    }

    protected inline fun <T> Observable<T>.subscribeUntilDetach(noinline onNext: (T) -> Unit) {
        subscribe(onNext, {}).disposeOnDetach()
    }
}