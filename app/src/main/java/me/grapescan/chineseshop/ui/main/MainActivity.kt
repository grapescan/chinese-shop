package me.grapescan.chineseshop.ui.main

import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import me.grapescan.chineseshop.R
import me.grapescan.chineseshop.analytics.AppAnalytics
import me.grapescan.chineseshop.analytics.types.ScreenType
import me.grapescan.chineseshop.app.AppRaterUtil
import me.grapescan.chineseshop.app.Injector
import me.grapescan.chineseshop.core.SendLogIntentFactory
import me.grapescan.chineseshop.data.RawProductSet
import me.grapescan.chineseshop.ui.base.BaseActivity
import me.grapescan.chineseshop.ui.product.list.ProductListActivity

class MainActivity : BaseActivity<MainView>(), MainView {

    override val presenter = Injector.mainPresenter
    private val listAdapter = ProductSetAdapter(presenter::onProductSetClick, presenter::onFeedbackClick)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        // TODO: add spacing around cards
        // TODO: preserve list position
        setList.adapter = listAdapter

        AppRaterUtil.onAppLaunch(this)
    }

    override fun onResume() {
        super.onResume()
        AppAnalytics.trackAppBrowse()
                .putScreen(ScreenType.main())
                .track()
    }

    override fun showProductSets(data: List<RawProductSet>) {
        listAdapter.submitList(data)
    }

    override fun openProductList(productSetId: String, screenColor: Int, textColor: Int) {
        ProductListActivity.start(this, productSetId, screenColor, textColor)
    }

    override fun openFeedbackMail() {
        startActivity(SendLogIntentFactory.getInstance(this).createGMailIntent())
    }
}
