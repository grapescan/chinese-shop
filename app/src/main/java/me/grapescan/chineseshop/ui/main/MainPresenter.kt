package me.grapescan.chineseshop.ui.main

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import me.grapescan.chineseshop.data.ProductRepository
import me.grapescan.chineseshop.data.RawProductSet
import me.grapescan.chineseshop.ui.base.Presenter

/**
 * @author Dmitry Soldatov
 */
class MainPresenter(private val repository: ProductRepository) : Presenter<MainView>() {

    override fun attachView(viewParam: MainView) {
        super.attachView(viewParam)
        repository.getProductSets()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeUntilDetach {
                    view?.showProductSets(it)
                }
    }

    fun onProductSetClick(productSet: RawProductSet, screenColor: Int, textColor: Int) {
        view?.openProductList(productSet.id, screenColor, textColor)
    }

    fun onFeedbackClick() {
        view?.openFeedbackMail()
    }
}