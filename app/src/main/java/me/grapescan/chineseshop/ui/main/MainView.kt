package me.grapescan.chineseshop.ui.main

import me.grapescan.chineseshop.data.RawProductSet

/**
 * @author Dmitry Soldatov
 */
interface MainView {
    fun showProductSets(data: List<RawProductSet>)
    fun openProductList(productSetId: String, screenColor: Int, textColor: Int)
    fun openFeedbackMail()

}