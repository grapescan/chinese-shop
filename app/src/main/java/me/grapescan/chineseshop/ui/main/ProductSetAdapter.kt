package me.grapescan.chineseshop.ui.main

import android.support.v7.graphics.Palette
import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_footer.view.*
import kotlinx.android.synthetic.main.item_product_set.view.*
import me.grapescan.chineseshop.R
import me.grapescan.chineseshop.core.centerCrop
import me.grapescan.chineseshop.core.getColor
import me.grapescan.chineseshop.data.RawProductSet
import me.grapescan.chineseshop.ui.base.BaseDiffCallback

/**
 * @author Dmitry Soldatov
 */
class ProductSetAdapter(
        private val itemClickListener: (RawProductSet, Int, Int) -> Unit,
        private val footerClickListener: () -> Unit
) : ListAdapter<RawProductSet, ProductSetAdapter.ViewHolder>(BaseDiffCallback<RawProductSet>()) {

    companion object {
        private const val TYPE_ITEM = 0
        private const val TYPE_FOOTER = 1
    }

    override fun getItemViewType(position: Int) = when {
        itemCount > 0 && position == itemCount - 1 -> TYPE_FOOTER
        else -> TYPE_ITEM
    }

    override fun getItemCount(): Int {
        val originalSize = super.getItemCount()
        return if (originalSize == 0) 0 else originalSize + 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return when (viewType) {
            TYPE_ITEM -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_product_set, parent, false)
                ProductSetViewHolder(view, itemClickListener)
            }
            TYPE_FOOTER -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_footer, parent, false)
                FooterViewHolder(view, footerClickListener)
            }
            else -> throw IllegalStateException("Illegal view type: $viewType")
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        when (holder) {
            is ProductSetViewHolder -> holder.bind(getItem(position))
        }
    }

    abstract class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    class ProductSetViewHolder(view: View, private val clickListener: (RawProductSet, Int, Int) -> Unit) : ViewHolder(view) {

        private val defaultPanelColor = getColor(R.color.panel_bg)
        private val defaultTitleColor = getColor(R.color.primary_text)
        private val defaultTextColor = getColor(R.color.subheader)

        var panelColor = defaultPanelColor
        var titleColor = defaultTitleColor
        var textColor = defaultTextColor

        private val title = itemView.productSetTitle
        private val description = itemView.productSetDescription
        private val size = itemView.productSetSize
        private val timestamp = itemView.productSetTimestamp
        private val image = itemView.productSetImage
        private val panel = itemView.productSetPanel

        fun bind(item: RawProductSet) {
            title.text = item.name
            description.text = item.description
            size.text = itemView.context.resources.getQuantityString(R.plurals.ideas_count, item.items.size, item.items.size)
            timestamp.text = ""//DateUtils.getRelativeTimeSpanString(item.date.time)
            image.centerCrop(item.coverImageUrl) { Palette.Builder(it).generate(::applyPalette) }
            itemView.setOnClickListener { clickListener.invoke(item, panelColor, titleColor) }
        }

        private fun applyPalette(palette: Palette) {
            panelColor = defaultPanelColor
            titleColor = defaultTitleColor
            textColor = defaultTextColor
            val swatchOrder = arrayOf(
                    palette.lightMutedSwatch,
                    palette.mutedSwatch,
                    palette.darkMutedSwatch,
                    palette.lightVibrantSwatch,
                    palette.vibrantSwatch,
                    palette.darkVibrantSwatch,
                    Palette.Swatch(defaultPanelColor, 100))
            for (swatch in swatchOrder) {
                try {
                    if (swatch != null) {
                        panelColor = swatch.rgb
                        textColor = swatch.titleTextColor
                        titleColor = swatch.bodyTextColor
                        break
                    }
                } catch (e: IllegalArgumentException) {
                    panelColor = defaultPanelColor
                    titleColor = defaultTitleColor
                    textColor = defaultTextColor
                }

            }
            setColors(panelColor, titleColor, textColor)
        }

        private fun setColors(panelColor: Int, titleColor: Int, textColor: Int) {
            panel.setBackgroundColor(panelColor)
            title.setTextColor(titleColor)
            description.setTextColor(textColor)
            size.setTextColor(textColor)
            timestamp.setTextColor(textColor)
        }
    }

    class FooterViewHolder(view: View, footerClickListener: () -> Unit) : ViewHolder(view) {
        init {
            itemView.feedbackButton.setOnClickListener { footerClickListener.invoke() }
        }
    }
}