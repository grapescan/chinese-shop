package me.grapescan.chineseshop.ui.model

/**
 * @author Dmitry Soldatov
 */
data class Product(
        val id: String = "",
        val title: String = "",
        val description: String = "",
        val price: String = "",
        val imageUrl: String = "",
        val url: String = ""
)