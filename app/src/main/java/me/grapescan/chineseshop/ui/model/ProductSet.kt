package me.grapescan.chineseshop.ui.model

import java.util.*

/**
 * @author Dmitry Soldatov
 */
data class ProductSet(
        val id: String,
        val title: String,
        val description: String,
        val imageUrl: String,
        val date: Date,
        val products: List<Product>
)