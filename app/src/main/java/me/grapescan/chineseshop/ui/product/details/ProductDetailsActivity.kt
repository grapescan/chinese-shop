package me.grapescan.chineseshop.ui.product.details

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.partial_product_details.*
import me.grapescan.chineseshop.R
import me.grapescan.chineseshop.analytics.AppAnalytics
import me.grapescan.chineseshop.analytics.types.ScreenType
import me.grapescan.chineseshop.app.Injector
import me.grapescan.chineseshop.ui.base.BaseActivity
import me.grapescan.chineseshop.ui.model.Product
import ru.tinkoff.mortgage.sdk.delegates.StringExtra

/**
 * @author Dmitry Soldatov
 */
class ProductDetailsActivity : BaseActivity<ProductDetailsView>(), ProductDetailsView {

    companion object {

        private var Intent.productId by StringExtra()

        @JvmStatic
        fun start(context: Context, productId: String) {
            context.startActivity(Intent(context, ProductDetailsActivity::class.java).apply {
                this.productId = productId
            })
        }
    }

    override val presenter = Injector.productDetailsPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_details)
        presenter.productId = intent.productId!!
        productBack.setOnClickListener { finish() }
    }

    override fun onResume() {
        super.onResume()
        AppAnalytics.trackAppBrowse()
                .putScreen(ScreenType.productDetails())
                .track()
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }

    override fun showProduct(product: Product) {
        productTitle.text = product.title
        productDescription.text = product.description
        productPrice.text = product.price
        Glide.with(productPhoto)
                .load(product.imageUrl)
                .apply(RequestOptions.centerInsideTransform())
                .into(productPhoto)
        productPurchase.setOnClickListener { openUrl(product.url) }
    }

    private fun openUrl(url: String) {
        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
    }

}