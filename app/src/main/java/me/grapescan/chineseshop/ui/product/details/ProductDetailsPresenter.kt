package me.grapescan.chineseshop.ui.product.details

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import me.grapescan.chineseshop.data.ProductRepository
import me.grapescan.chineseshop.ui.base.Presenter

/**
 * @author Dmitry Soldatov
 */
class ProductDetailsPresenter(private val repository: ProductRepository) : Presenter<ProductDetailsView>() {

    lateinit var productId: String

    override fun attachView(viewParam: ProductDetailsView) {
        super.attachView(viewParam)
        repository.getProduct(productId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeUntilDetach {
                    view?.showProduct(it)
                }
    }
}