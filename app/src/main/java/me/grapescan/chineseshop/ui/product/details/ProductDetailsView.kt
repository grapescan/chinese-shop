package me.grapescan.chineseshop.ui.product.details

import me.grapescan.chineseshop.ui.model.Product

/**
 * @author Dmitry Soldatov
 */
interface ProductDetailsView {

    fun showProduct(product: Product)
}