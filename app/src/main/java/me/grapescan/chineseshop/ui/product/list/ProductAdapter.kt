package me.grapescan.chineseshop.ui.product.list

import android.support.v7.graphics.Palette
import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_product.view.*
import me.grapescan.chineseshop.R
import me.grapescan.chineseshop.core.centerInside
import me.grapescan.chineseshop.core.getColor
import me.grapescan.chineseshop.ui.base.BaseDiffCallback
import me.grapescan.chineseshop.ui.model.Product

/**
 * @author Dmitry Soldatov
 */
class ProductAdapter(private val clickListener: (Product) -> Unit) : ListAdapter<Product, ProductAdapter.ProductViewHolder>(BaseDiffCallback<Product>()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_product, parent, false)
        return ProductViewHolder(view, clickListener)
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class ProductViewHolder(view: View, val clickListener: (Product) -> Unit) : RecyclerView.ViewHolder(view) {

        private val defaultButtonColor = getColor(R.color.button_bg)
        private val defaultTextColor = getColor(android.R.color.white)

        private val title = itemView.productTitle
        private val description = itemView.productDescription
        private val photo = itemView.productPhoto
        private val purchase = itemView.productPurchase
        private val price = itemView.productPrice

        fun bind(item: Product) {
            title.text = item.title
            description.text = item.description
            price.text = item.price
            photo.centerInside(item.imageUrl) { Palette.Builder(it).generate(::applyPalette) }
            itemView.setOnClickListener { clickListener.invoke(item) }
        }

        private fun applyPalette(palette: Palette) {
            var buttonColor = defaultButtonColor
            var textColor = defaultTextColor
            val swatchOrder = arrayOf(
                    palette.mutedSwatch,
                    palette.vibrantSwatch,
                    palette.darkVibrantSwatch,
                    palette.darkMutedSwatch,
                    palette.lightVibrantSwatch,
                    palette.lightMutedSwatch,
                    Palette.Swatch(defaultButtonColor, 100))
            for (swatch in swatchOrder) {
                try {
                    if (swatch != null) {
                        buttonColor = swatch.rgb
                        textColor = swatch.titleTextColor
                        break
                    }
                } catch (e: IllegalArgumentException) {
                    buttonColor = defaultButtonColor
                    textColor = defaultTextColor
                }

            }
            setColors(buttonColor, textColor)
        }

        private fun setColors(buttonColor: Int, textColor: Int) {
            purchase.setBackgroundColor(buttonColor)
            purchase.setTextColor(textColor)
            title.setTextColor(buttonColor)
        }
    }
}