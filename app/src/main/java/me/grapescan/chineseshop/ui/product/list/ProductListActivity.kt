package me.grapescan.chineseshop.ui.product.list

import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.PorterDuff
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.View
import android.widget.Toast
import com.yarolegovich.discretescrollview.InfiniteScrollAdapter
import com.yarolegovich.discretescrollview.transform.Pivot
import com.yarolegovich.discretescrollview.transform.ScaleTransformer
import kotlinx.android.synthetic.main.activity_product_set.*
import me.grapescan.chineseshop.R
import me.grapescan.chineseshop.analytics.AppAnalytics
import me.grapescan.chineseshop.analytics.types.ScreenType
import me.grapescan.chineseshop.app.Injector
import me.grapescan.chineseshop.core.open
import me.grapescan.chineseshop.ui.base.BaseActivity
import me.grapescan.chineseshop.ui.model.ProductSet
import ru.tinkoff.mortgage.sdk.delegates.IntExtra
import ru.tinkoff.mortgage.sdk.delegates.StringExtra

/**
 * @author Dmitry Soldatov
 */
class ProductListActivity : BaseActivity<ProductListView>(), ProductListView {

    companion object {

        private var Intent.productSetId by StringExtra()
        private var Intent.screenColor by IntExtra()
        private var Intent.textColor by IntExtra()

        @JvmStatic
        fun start(context: Context, productSetId: String, screenColor: Int, textColor: Int) {
            context.startActivity(Intent(context, ProductListActivity::class.java).apply {
                this.productSetId = productSetId
                this.screenColor = screenColor
                this.textColor = textColor
            })
        }
    }

    override val presenter = Injector.productListPresenter
    private val listAdapter = ProductAdapter(presenter::onProductClick)
    private val infiniteListAdapter = InfiniteScrollAdapter.wrap(listAdapter)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        intent?.productSetId?.let { presenter.productSetId = it }
        presenter.onViewReload(listAdapter.itemCount)

        setContentView(R.layout.activity_product_set)
        initColors(intent.screenColor, blendColors(intent.textColor, intent.screenColor, 0.5f))
        toolbar.setNavigationOnClickListener { finish() }
        initList()
    }

    private fun initList() {
        itemsList.adapter = infiniteListAdapter
        itemsList.setOffscreenItems(2)
        itemsList.setItemTransformer(ScaleTransformer.Builder()
                .setMaxScale(1.0f)
                .setMinScale(0.9f)
                .setPivotX(Pivot.X.CENTER) // CENTER is a default one
                .setPivotY(Pivot.Y.CENTER) // CENTER is a default one
                .build())
    }

    private fun initColors(screenColor: Int, textColor: Int) {
        container.setBackgroundColor(screenColor)
        val closeIcon = ContextCompat.getDrawable(this, R.drawable.ic_close_black_24dp)!!.apply {
            setColorFilter(textColor, PorterDuff.Mode.SRC_ATOP)
        }
        toolbar.setNavigationIcon(closeIcon)
        toolbar.setTitleTextColor(textColor)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progress.indeterminateTintList = ColorStateList.valueOf(textColor)
        }
    }

    override fun onResume() {
        super.onResume()
        AppAnalytics.trackAppBrowse()
                .putScreen(ScreenType.productDetails())
                .track()
    }

    override fun showProductSet(data: ProductSet) {
        toolbar.title = data.title
        listAdapter.submitList(data.products)
        infiniteListAdapter.notifyDataSetChanged()
    }

    override fun showError(error: Throwable) {
        Log.e("ProductList", "unexpected error", error)
        Toast.makeText(this, error.message, Toast.LENGTH_SHORT).show()
    }

    override fun showProgress() {
        progress.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progress.visibility = View.GONE
    }

    override fun openProduct(url: String) {
        AppAnalytics.trackAppBrowse()
                .putScreen(ScreenType.browser())
                .track()
        //startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
        Uri.parse(url).open(this)
    }

    private fun blendColors(color1: Int, color2: Int, ratio: Float): Int {
        val inverseRation = 1f - ratio
        val r = Color.red(color1) * ratio + Color.red(color2) * inverseRation
        val g = Color.green(color1) * ratio + Color.green(color2) * inverseRation
        val b = Color.blue(color1) * ratio + Color.blue(color2) * inverseRation
        return Color.rgb(r.toInt(), g.toInt(), b.toInt())
    }

}