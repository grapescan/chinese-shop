package me.grapescan.chineseshop.ui.product.list

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import me.grapescan.chineseshop.data.ProductRepository
import me.grapescan.chineseshop.ui.base.Presenter
import me.grapescan.chineseshop.ui.model.Product

/**
 * @author Dmitry Soldatov
 */
class ProductListPresenter(private val repository: ProductRepository) : Presenter<ProductListView>() {

    lateinit var productSetId: String

    fun onProductClick(product: Product) {
        view?.openProduct(product.url)
    }

    fun onViewReload(displayedItemsCount: Int) {
        if (displayedItemsCount > 0) {
            return
        }
        view?.showProgress()
        repository.getProductSet(productSetId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeUntilDetach({
                    view?.hideProgress()
                    view?.showProductSet(it)
                }, {
                    view?.hideProgress()
                    view?.showError(it)
                })
    }
}