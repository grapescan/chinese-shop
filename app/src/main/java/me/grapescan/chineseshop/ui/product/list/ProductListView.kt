package me.grapescan.chineseshop.ui.product.list

import me.grapescan.chineseshop.ui.model.ProductSet

/**
 * @author Dmitry Soldatov
 */
interface ProductListView {

    fun openProduct(url: String)
    fun showProductSet(data: ProductSet)
    fun showProgress()
    fun hideProgress()
    fun showError(error: Throwable)
}