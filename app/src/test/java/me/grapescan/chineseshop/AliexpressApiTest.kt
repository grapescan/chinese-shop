package me.grapescan.chineseshop

import com.google.gson.GsonBuilder
import me.grapescan.chineseshop.api.aliexpress.AliexpressApi
import me.grapescan.chineseshop.api.base.PayloadConverterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory

class AliexpressApiTest {

    protected val retrofit: Retrofit by lazy {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        val httpClient = OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .build()
        val gson = GsonBuilder()
                .create()
        Retrofit.Builder()
                .baseUrl("http://gw.api.alibaba.com")
                .client(httpClient)
                .addConverterFactory(PayloadConverterFactory(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }

    @Test
    fun testHotProductList() {
        val api = retrofit.create(AliexpressApi::class.java)
        api.getListHotProducts(83490, "ru", "RUB", "1420").test().apply {
            assertNoErrors()
            assertValueCount(1)
            assertValue { it.products.isNotEmpty() }
        }
    }
}
