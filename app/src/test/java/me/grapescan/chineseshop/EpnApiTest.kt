package me.grapescan.chineseshop

import me.grapescan.chineseshop.api.epn.EpnApi
import me.grapescan.chineseshop.api.epn.model.EpnRequest
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class EpnApiTest {

    protected val retrofit: Retrofit by lazy {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        val httpClient = OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .build()
        Retrofit.Builder()
                .baseUrl("http://api.epn.bz/")
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }

    @Test
    fun testOfferInfo() {
        val api = retrofit.create(EpnApi::class.java)
        val request = EpnRequest(requestsToProcess = listOf(mapOf(
                "action" to "offer_info",
                "lang" to "en",
                "id" to "32852750858",
                "currency" to "RUR,USD"
        )))
        api.request(request).test().apply {
            assertNoErrors()
            assertValueCount(1)
            assertValue { it.results.first().offer?.productId == "32852750858" }
        }
    }
}
